darwin_platform := macosx

include rebuild/port-darwin-core.mk

CPPFLAGS += -mmacosx-version-min=$(ReBuild_sdkVer)
CFLAGS += -mmacosx-version-min=$(ReBuild_sdkVer)
LDFLAGS += -mmacosx-version-min=$(ReBuild_sdkVer)
