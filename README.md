[[_TOC_]]

# Overview #
ReBuild is a non-recursive, GNU Make based, build system designed to:

1. Be FAST
2. Require no dependencies other than GNU Make
3. Be declarative
4. Support cross-compiling
5. Support build-time generated code
6. Reduce reptition
7. Support git submodules and other external projects

# Requirements #
- GNU Make 3.81 or newer
- An sh compliant shell (developed using bash)

# Why Another Build System? #
Well, I'm dissatisfied with my options. I do my software development in Vim,
where possible, and I write a lot of code to be cross compiled for various \*nix
systems or bare-metal. As such, the nice, native build sytems and tools like
Visual Studio and XCode don't really meet my needs. Many other command line
build systems are brutally slow - Maven, SCons, Gradle, etc. - and I really
don't have patience for that since I like to compile and test often. CMake was
briefly promising, until I realized it has 22 dependencies, the syntax is
arcane, and, on most of the platforms I care about, it reduces to a Makefile
anyway.

So, I went back to make. Vim has built in support for executing make, if you can
use make properly, make supports parallel builds and incremental builds, make is
blazing fast, CMake reduces to make anyway, and, with support for Windows
Subsystem for Linux on the rise, I'm hopeful make will work well on Windows
someday.

The only problems I have with make are the arcane syntax, the difficulty in
keeping Makefiles up to date, and the fact that I end up writing very similar
Makefiles for every project - repeating tons of complex logic that I can never
remember how to write becuase, as I mentioned, the syntax is arcane. So I set
out to try and build a set of reusable Makefiles and ReBuild was born.

ReBuild handles the complexity of managing dependencies for you, and, for most
compilations, eliminates the need to understand the vast majorty of make syntax.
Make variable assignment and usage are the only elements of make you really need
to know to get started, turning your Makefiles from complex magic into simple,
declarative Makefile fragments. There's always the chance you will need to write
your own custom build rules as your project gets more and more complex, but
that should be the exception not the rule. Even adding a new toolchain is
declarative - simply fill in the toolchain variables and you're ready to build.

The goal for ReBuild is to know how to build the things I want to build using a
simple command line command - Python wheels, Docker images, C/C++ programs,
Swift applications, and more. It's not intended to become a package management
system and will likely never replace language-specific build tools like Go get,
Rust crates, and Maven (though, I'm open to disagreements from language experts
since I don't do any meaningful development in Go, Rust, or Java). ReBuild
should also help me cross compile, manage build output, and work well in
continuous integration environments.

# How it Works
ReBuild is built around GNU Make, so the only thing you should have to do to
build your project is type:

  ```bash
  make
  ```

ReBuild puts all built artifacts into one of two places. Main artifacts like
applications, libraries, and public header files are put into a directory tree
at the root level of your application under the build/ directory. ReBuild is
designed to enable compiling your code for multiple targets without changing
directories, so the build/ directory has parallel subdirectories for the
different target architectures, toolchains, and build tags. The general struture
is:

    build/<arch>-<os>-<toolchain>/<tag>/
        build.log       - build log
        inc/            - public header files
        release/        - root of a single build 'tag'
            bin/        - built applications
            lib/        - built libraries
            pkg/        - built external packages

Additionally, if you have built extenal projects, there may be more directories
under the build tag (release/ in the example above) reflecting other things your
external projects have "installed" (like man pages).

The second place ReBuild will store files is in a build/ tree under each module.
The only things in the module-specific build/ trees are module-specific
intermediate files like object files and dependency files. This helps reduce
conflicts across independently developed projects/modules (i.e. if two modules
have a file called crypt.c, they each have their own crypt.o too).

ReBuild first compiles external projects. Since ReBuild doesn't understand
everything about every external build tool, it can't anticipate which
dependencies will be created by those projects, so the first thing it does is
build them. There is a minor ineffeciency here when doing parallel builds (i.e.
make -j) in that none of your project code will be compiled until all external
projects are built. However, external projects will still be built in parallel
where possible.

ReBuild then "installs" the external projects. If you're building an Autotools
project, ReBuild takes care of changing the install prefix to the right location
under build/ so that Autotools publishes the build artifacts and headers
properly.

Next, ReBuild publishes any of your project's "public" headers. These are the
headers required by other libraries in your project.

Finally, ReBuild compiles your project code.

# Built-In Build Targets
ReBuild comes with some pre-defined build targets.

- clean - Typing 'make clean' will forcibly delete any directory named build/
  anywhere in your build tree.
- help - Typing 'make help' will show you command line options and available
  toolchains as well as some basic usage information.
- template - Typing 'make -f rebuild/rebuild.mk template' will create a skeleton
  Makefile and add the build/ directories to your gitignore (if a git repository
  is detected).

# Command Line Options
ReBuild is really just GNU Make, so you can override nearly any internal
variable on the command line. I suggest against overriding most variables this
way as you may affect the build system in unexpected ways. However, there are a
few variables you are expected to modify.

## Verbosity
ReBuild uses the variable 'quiet' to control the verbosity. quiet defaults to
'yes', meaning you get minimal build output, highlighting any compile warnings
or errors you may have. To see every build step in every recipe, you can set
quiet to 'no'. Note that regardless of the verbosity, all build commands and
results are logged to build.log in the build/ directory.

  ```bash
  make quiet=no
  ```

## Build Tags
ReBuild supports a variable called 'tag' to switch between debug, release, and
debugrelease builds. ReBuild defaults to release builds, but you an override it
on the command line.

  ```bash
  make tag=debug
  make tag=debugrelease
  ```

It is generally expected that release builds contain no debug code and enable
full optimizations - i.e. anything you'd need to 'release' your project. Debug
builds will tend to enable debug code - logs, asserts, etc. - and may disable
optimizations for easier debugging. Debugrelease builds are a hybrid. They
enable both debug and release code as well as enable optimizations. The primary
usecase for this is addressing issues in your code which may be caused by
enabling certain compiler optimizations.

## Cross Compiling
ReBuild uses the variables 'arch', 'os', 'vendor', and 'toolchain' to specify
which compiler to use to build your code. If left unspecified, 'vendor' is left
unset and the remainder of the variables are guessed from your current
environment.

  ```bash
  make arch=arm os=linux toolchain=clang
  ```

arch, os, vendor, and toolchain directly map to a file in the rebuild directory
named port-\<arch\>-\<vendor\>-\<os\>-\<toolchain\>.mk which contains the
configuration for that specific toolchain.

## Overrides
Alternately, you can override any of the above variables in your root Makefile
before including rebuild/rebuild.mk if you want to change the defaults or you
only want to build for a single platform. For example, if you'd prefer to
default to building debug builds for ARM, you could do the following:

  ```make
  ReBuild_project := myapp
  tag ?= debug
  arch ?= arm

  ...

  include rebuild/rebuild.mk
  ```

Additionally, if you absolutely hate my choice to build release builds by
default, the tag variable default can be changed on your system globally using
the environment variable REBUILD\_DEFAULT\_TAG.

  ```bash
  > make
  Building myapp for x86_64-darwin/release
  > export REBUILD_DEFAULT_TAG=debug
  > make
  Building myapp for x86_64-darwin/debug
  ```

# API
[Read about the user API](docs/variables.md)

# How To Guides

* [How to compile a C file into an application](docs/quickstart.md)
* [How to use sub modules/libraries](docs/submodules.md)
* [How to compile Autotools projects](docs/external-autotools.md)
* How to auto-generate source files
* How to customize source and include directories
* [How to cross-compile](docs/cross-compiling.md)
* [How to add a new toolchain](docs/porting.md)

