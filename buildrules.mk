srcdir := $(if $(srcdir),$(srcdir),.)
incdir := $(if $(incdir),$(incdir),.)

cfiles := \
  $(if $(cfiles),$(cfiles),$(notdir $(wildcard $(cpath)/$(srcdir)/*.c))) \
  $(notdir $(cfilesgen))
cppfiles := \
  $(if $(cppfiles),$(cppfiles),$(notdir $(wildcard $(cpath)/$(srcdir)/*.cpp))) \
  $(notdir $(cppfilesgen))
cxxfiles := \
  $(if $(cxxfiles),$(cxxfiles),$(notdir $(wildcard $(cpath)/$(srcdir)/*.cxx))) \
  $(notdir $(cxxfilesgen))

ofiles := $(addprefix $(cpath)/$(ReBuild_buildPathObj)/,$(cfiles:.c=.o)) \
          $(addprefix $(cpath)/$(ReBuild_buildPathObj)/,$(cppfiles:.cpp=.o)) \
          $(addprefix $(cpath)/$(ReBuild_buildPathObj)/,$(cxxfiles:.cxx=.o))
dfiles := $(ofiles:.o=.d)

VPATH := $(VPATH) $(cpath)/$(srcdir)

################################################################################
# Header Files
################################################################################
hfiles := $(hfiles) $(notdir $(hfilesgen))

#$(info $(call ReBuild_publishHeader,$(cpath),$(hfilesdir),$(incdir)))
$(eval $(call ReBuild_publishHeader,$(cpath),$(hfilesdir),$(incdir)))

publicHeaders := $(addprefix $(ReBuild_buildPathInc)/$(hfilesdir)/,$(notdir $(hfiles))) $(hfilesgen) 
all: $(publicHeaders)
ReBuild_allPublicHeaders := $(ReBuild_allPublicHeaders) $(publicHeaders)

################################################################################
# Object Files and Dependency Files
################################################################################
$(cpath)/$(ReBuild_buildPathObj)/%.o: CPPFLAGS := $(CPPFLAGS) $(extraCPPFLAGS)
$(cpath)/$(ReBuild_buildPathObj)/%.o: CFLAGS := $(CFLAGS) $(extraCFLAGS)

$(cpath)/$(ReBuild_buildPathObj)/%.o: $(cpath)/$(srcdir)/%.c $(ReBuild_allPkgTargets) $(ReBuild_allPublicHeaders) $(cpath)/rebuild.mk $$(@D)/.marker
	$(q)$(call log,compile,$<)
	$(q)$(call run,$(CC) $(CPPFLAGS) -std=${cstd} $(CFLAGS) -MMD -c $< -o $@)

$(cpath)/$(ReBuild_buildPathObj)/%.o: $(cpath)/$(srcdir)/%.cpp $(ReBuild_allPkgTargets) $(ReBuild_allPublicHeaders) $(cpath)/rebuild.mk $$(@D)/.marker
	$(q)$(call log,compile,$<)
	$(q)$(call run,$(CXX) $(CPPFLAGS) -std=${cxxstd} $(CXXFLAGS) -MMD -c $< -o $@)

$(cpath)/$(ReBuild_buildPathObj)/%.o: $(cpath)/$(srcdir)/%.cxx $(ReBuild_allPkgTargets) $(ReBuild_allPublicHeaders) $(cpath)/rebuild.mk $$(@D)/.marker
	$(q)$(call log,compile,$<)
	$(q)$(call run,$(CXX) $(CPPFLAGS) -std=${cxxstd} $(CXXFLAGS) -MMD -c $< -o $@)

ifneq ($(MAKECMDGOALS),clean)
  -include $(dfiles)
endif

################################################################################
# Static Libraries
################################################################################
ifneq ($(staticLibName),)

$(ReBuild_buildPathLib)/lib$(staticLibName).a: $(ofiles) $$(@D)/.marker
	$(q)$(call log,archive,$@)
	$(q)$(call run,$(AR) -rc $@ $(filter %.o,$^))

ReBuild_allLibs := $(ReBuild_allLibs) $(ReBuild_buildPathLib)/lib$(staticLibName).a

all: $(ReBuild_buildPathLib)/lib$(staticLibName).a

endif	# staticLibName

################################################################################
# Shared Libraries
################################################################################
# _ldflag := -dylib -install_name $(_relativeTargetPath)
# _ldflag := -shared
ifneq ($(sharedLibName),)

libdeps := $(filter $(addprefix %/lib,$(addsuffix .a,$(staticLibs))),$(ReBuild_allLibs))

$(ReBuild_buildPathLib)/lib$(sharedLibName).$(soext): LDFLAGS := $(LDFLAGS) $(extraLDFLAGS)
$(ReBuild_buildPathLib)/lib$(sharedLibName).$(soext): staticLibs := $(staticLibs)
$(ReBuild_buildPathLib)/lib$(sharedLibName).$(soext): sharedLibs := $(sharedLibs)

$(ReBuild_buildPathLib)/lib$(sharedLibName).$(soext).dirty: $(ofiles) $(libdeps) $$(@D)/.marker
	$(q)$(call log,link,$@)
	$(q)$(CC) $(LDFLAGS) \
		$(ReBuild_ldSoFlags) \
		$(filter %.o,$^) \
		-L$(ReBuild_buildPathLib) \
		$(addsuffix .a,$(addprefix -l:lib,$(staticLibs))) \
		$(addprefix -l,$(sharedLibs)) \
		-o $@

$(ReBuild_buildPathLib)/lib$(sharedLibName).$(soext).$(symext): $$(@D)/lib$(sharedLibName).$(soext).dirty
	$(q)$(call log,syms,$@)
	$(q)$(call run,$(call ReBuild_extractSymbols,$<,$@))

$(ReBuild_buildPathLib)/lib$(sharedLibName).$(soext): $$(@D)/lib$(sharedLibName).$(soext).dirty $$(@D)/lib$(sharedLibName).$(soext).$(symext)
	$(q)$(call log,strip,$<)
	$(q)$(call run,$(STRIP) $< -o $@)
	$(q)$(call run,$(call ReBuild_linkSymbols,$(lastword $^),$@))

ReBuild_allLibs := $(ReBuild_allLibs) $(ReBuild_buildPathLib)/lib$(sharedLibName).$(soext)

all: $(ReBuild_buildPathLib)/lib$(sharedLibName).$(soext)

endif	# sharedLibName

################################################################################
# Executables
################################################################################
ifneq ($(appName),)

libdeps := $(filter $(addprefix %/lib,$(addsuffix .a,$(staticLibs))),$(ReBuild_allLibs))
libdeps += $(filter $(addprefix %/lib,$(addsuffix .$(soext),$(sharedLibs))),$(ReBuild_allLibs))

$(ReBuild_buildPathBin)/$(appName): LDFLAGS := $(LDFLAGS) $(extraLDFLAGS)
$(ReBuild_buildPathBin)/$(appName): staticLibs := $(staticLibs)
$(ReBuild_buildPathBin)/$(appName): sharedLibs := $(sharedLibs)

$(ReBuild_buildPathBin)/$(appName).dirty: $(ofiles) $(libdeps) $$(@D)/.marker \
                                                    $(ReBuild_buildPathLib)/.marker
	$(q)$(call log,link,$@)
	$(q)$(CC) $(LDFLAGS) \
		$(filter %.o,$^) \
		-L$(ReBuild_buildPathLib) \
		$(addsuffix .a,$(addprefix -l:lib,$(staticLibs))) \
		$(addprefix -l,$(sharedLibs)) \
		-o $@

ifeq ($(symext),dSYM)

$(ReBuild_buildPathBin)/$(appName): $$(@D)/$(appName).dirty
	$(q)$(call log,strip,$@)
	$(q)$(call run,$(STRIP) $< -o $@)

$(ReBuild_buildPathBin)/$(appName).$(symext)/.marker : $(ReBuild_buildPathBin)/$(appName).dirty
	$(q)$(call log,syms,$(@D))
	$(q)$(call run,$(call ReBuild_extractSymbols,$<,$(@D)))
	$(q)$(call run,touch $@)

all: $(ReBuild_buildPathBin)/$(appName).$(symext)/.marker

else

$(ReBuild_buildPathBin)/$(appName).$(symext): $$(@D)/$(appName).dirty
	$(q)$(call log,syms,$@)
	$(q)$(call run,$(call ReBuild_extractSymbols,$<,$@))

$(ReBuild_buildPathBin)/$(appName): $$(@D)/$(appName).dirty $$(@D)/$(appName).$(symext)
	$(q)$(call log,strip,$@)
	$(q)$(call run,$(STRIP) $< -o $@)
	$(q)$(call run,$(call ReBuild_linkSymbols,$(lastword $^),$@))

all: $(ReBuild_buildPathBin)/$(appName)

endif	# dSYM vs debug

all: $(ReBuild_buildPathBin)/$(appName)

endif	# appName

srcdir :=
incdir :=
appName :=
staticLibName := 
sharedLibName := 
hfiles :=
hfilesgen :=
hfilesdir :=
cfiles :=
cfilesgen :=
cppfiles := 
cppfilesgen :=
cxxfiles := 
cxxfilesgen :=
staticLibs :=
sharedLibs :=

extraCPPFLAGS :=
extraCFLAGS :=
extraLDFLAGS :=

$(foreach cpath,$(addprefix $(cpath)/,$(dirs)),$(eval $(call ReBuild_recurse)))

