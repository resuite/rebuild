ifeq ($($(pkg)_TARGETS),)
  $(error $(pkg) defined without $(pkg)_TARGETS.)
endif

$(ReBuild_buildPathPkg)/$(pkg)/.marker: $(ReBuild_buildPathPkg)/.marker
	$(q)$(call log,sympkg,$(pkg))
	$(q)$(call run,ln -sf $(CURDIR)/$(pkg) $(@D) && touch $@)

$(addprefix $(ReBuild_buildPathPkg)/$(pkg)/,$($(pkg)_TARGETS)): pkg := $(pkg)
$(addprefix $(ReBuild_buildPathPkg)/$(pkg)/,$($(pkg)_TARGETS))&: $(ReBuild_buildPathPkg)/$(pkg)/.marker
	$(q)$(call log,compile,$(pkg))
	$(q)$(call run,cd $(<D); \
		$(MAKE) $($(pkg)_MAKETARGETS) $($(pkg)_ENV))

$(addprefix $(ReBuild_buildPathLib)/,$(notdir $($(pkg)_TARGETS))): pkg := $(pkg)
$(addprefix $(ReBuild_buildPathLib)/,$(notdir $($(pkg)_TARGETS)))&: $(addprefix $(ReBuild_buildPathPkg)/$(pkg)/,$($(pkg)_TARGETS))
	$(q)$(call log,publish,$(pkg))
	$(q)$(call run,cd $(ReBuild_buildPathPkg)/$(pkg); $(MAKE) install $($(pkg)_ENV))

ReBuild_allPkgTargets := $(ReBuild_allPkgTargets) $(addprefix $(ReBuild_buildPathLib)/,$(notdir $($(pkg)_TARGETS)))
#all: $(addprefix $(ReBuild_buildPathLib)/,$(notdir $($(pkg)_TARGETS)))
all: $($(pkg)_FIXUP_TARGETS)
