# Overview #
ReBuild is designed to make porting to gcc and clang-based compilers easy.

A port file has a canonical format of:

    port-<arch>-<vendor>-<os>-<toolchain>.mk

However, some of the fields in angle brackets (<>) can be omitted if necessary
or convenient. The general idea is that the build system can guess the name of
the port file by inspecting either the local environment or by using arguments
passed into the `make` invocation.

Currently, the following definitions are required to be set in a port file:
- CC - Path to the C compiler
- CPP - Path to the C preprocessor
- CXX - Path to the C++ compiler
- LD - Path to the linker
- AR - Path to the archiver (to make static libraries)
- STRIP - Path to the symbol stripper
- ReBuild\_extractSymbols - Function to extract and save debug symbols
- OBJCOPY - (For \*nix systems) Path to the objcopy binary

Other definitions can be provided if they help with development on a particular
platform or toolchain. For some of the above definitions, if they are left
unset, they will default to the current installed versions of those binaries. It
is recommended to set ALL of them when creating a port file which is not
intended to use the local toolchain.

# Example 1 - Local Linux GCC Toolchain #
As a first, simple example we'll look at the port file for the native
x86\_64-linux toolchain, replicated below.

  ```make
  CFLAGS += \
    -ffunction-sections \
    -fdata-sections \

  LDFLAGS += \
    -Wl,--gc-sections

  ReBuild_extractSymbols = objcopy --only-keep-debug $1 $2 && objcopy --add-gnu-debuglink=$2 $3

  ReBuild_ldSoFlags := -shared
  ReBuild_ldNameFlags := -Wl,soname

  STRIP := strip -s
  ```

Note that, in this case, many of the above, required variables are left unset.
This is because most of these variables have GNU make defaults and, if left
unset, those defaults will be used. Since this port file is used to build
software with the locally installed compiler, whatever it may be, we use those
make defaults.

GNU make suports the CFLAGS and LDFLAGS variables, but in our case we want to
enable link-time optimizations by default to reduce code bloat so we provide
default CFLAGS and LDFLAGS values.

The rest of the variables in the file have no GNU make defaults and os we must
specify them.

One quick note on the ReBuild\_extractSymbols macro. This macro gets invoked
with the final, unstripped binary as parameter 1, the location to store the
symbols as parameter 2, and the stripped binary as parameter 3. It then extracts
the debug symbols into their own file and adds a debug link to to it to the
final, stripped binary.

All in all, this is about as simple of a port file as one can get. It uses
whatever toolchain is accessible on the path and sets minimal override flags.

# Example 2 - MSP430 Open Source Toolchain #
As a second example, we'll look at the MSP430 toolchain. This toolchain comes as
a tar file from TI which contains all of the pre-built binaries and the sysroot.
In this case, we're going to have to specifically point ReBuild at all of the
individual tools. The portfile for msp430-elf-gcc is replicated below:

  ```make
  msp430_toolchain := /opt/msp430-elf-gcc

  CPPFLAGS += \
    -I$(msp430_toolchain)/include

  CFLAGS += \
    -Wl,--gc-sections \
    -mmcu=$(msp430_cpu) \
    -Os

  LDFLAGS += \
    -mmcu=$(msp430_cpu) \
    -L$(msp430_toolchain)/lib \
    -L$(msp430_toolchain)/include

  OBJCOPY := $(msp430_toolchain)/bin/msp430-elf-objcopy

  ReBuild_extractSymbols = $(OBJCOPY) --only-keep-debug $1 $2 && $(OBJCOPY) --add-gnu-debuglink=$2 $3

  CC := $(msp430_toolchain)/bin/msp430-elf-gcc
  CPP := $(msp430_toolchain)/bin/msp430-elf-cpp
  CXX := $(msp430_toolchain)/bin/msp430-elf-c++
  LD := $(msp430_toolchain)/bin/msp430-elf-ld
  AR := $(msp430_toolchain)/bin/msp430-elf-ar
  STRIP := $(msp430_toolchain)/bin/msp430-elf-strip
  GDB := $(msp430_toolchain)/bin/msp430-elf-gdb
  GDB_AGENT_CONSOLE := $(msp430_toolchain)/bin/gdb_agent_console
  ```

This file isn't too complicated either - which is one of the key designs of
ReBuild. Porting ReBuild to new gcc-based toolchains should be easy. This port
file looks very similar to the first example, except that we now specify every
one of the required variables. We also do a few more things:

This port file specifies a toolchain-specific variable called `msp430_toolchain`
which points to the base directory of the toolchain. While this port assumes
that the toolchain is located in /opt/msp430-elf-gcc, it's easy enough to
override this variable either on the command line or in the main project's
Makefile to point to any other installed MSP430 toolchain.

This port file also has another input variable called `msp430_cpu`. The MSP430
toolchain supports a wide variety of MSP430-based CPUs and needs to be told
which one to compile for. Again, this variable can be specified either on the
command line or in the project's Makefile. Specifying it in the Makefile makes
the most sense since a single project may not be getting built for multiple
microcontrollers.

Lastly, this port file defines the GDB and GDB\_AGENT\_CONSOLE variables to
point at two SDK-specific tools used by MSP430 developers.

