# Overview #
There are three types of variables defined in ReBuild:
1. Public, short-name variables and functions
2. Public, long-name variables and functions
3. Private variables

You're free to use any of them, but only the public variables are considered
part of the stable API and documented here. Private variables may change without
notice. All private variables are prefixed with ReBuild\_ in order to avoid
conflicts with your makefile fragments.

There is no difference in the use of short-name and long-name variables, only
what they're named. They're called out here separately because they appear
distinct. In fact, they're both part of the public, stable API. It simply became
too cumbersome to properly namespace some commonly used variables/functions, and
so they were given short, easy to use names. For example:

- lower - function to lowercase a word
- log - function to generate a formatted log message on stdout

Less commonly used variables are prefixed with the ReBuild\_ namespace.

- ReBuild\_buildPath - the base path to the built files

# User Variables #
These variables are set by the user to control how modules and projects are
built.

| Name | Used In | Description |
|------|---------|-------------|
| dirs | Makefile | A list of submodules to build, ordered with dependencies coming before the modules which depend on them. |
| pkgs | Makefile | A list of external packges to build. |
| \<pkg\>\_CFLAGS | Makefile | Package-specific CFLAGS to be added to the project CFLAGS |
| \<pkg\>\_CONFIGURE | Makefile | Package-specific configuration command (default: ./configure). Note that if you set this you do not get the default configure flags and must specify, at minimum, the equivilants of --prefix and --includedir. |
| \<pkg\>\_CONFIGURE\_FLAGS| Makefile | Package-specific configure flags. |
| \<pkg\>\_CPPFLAGS | Makefile | Package-specific CPPFLAGS to be added to the project CPPFLAGS |
| \<pkg\>\_LDFLAGS | Makefile | Package-specific LDFLAGS to be added to the project LDFLAGS |
| \<pkg\>\_TARGET | Makefile | Package artifacts you care about being built. a
| ReBuild\_project | Makefile | Your project name. |
| srcdir | rebuild.mk | Relative path to a module's source files (default: ./). |
| incdir | rebuild.mk | Relative path to a module's include files (default: ./). |
| cfiles | rebuild.mk | List of C files to compile. (default: all .c files in 'srcdir') |
| cppfiles | rebuild.mk | List of C++ files to compile. (default: all .cpp files in 'srcdir') |
| cxxfiles | rebuild.mk | List of C++ files to compile. (default: all .cxx files in 'srcdir') |
| hfiles | rebuild.mk | List of header files to publish to the build/ tree. (default: none) |
| staticLibName | rebuild.mk | If set, builds the module into a static library (.a) with this name. |
| staticLibs | rebuild.mk | A list of static libraries to link into this module, ordered for a single-pass linker (dependencies after libraries which depend on them). |
| sharedLibName | rebuild.mk | If set, builds the module into a dynamic library (.so, .dylib) with this name. |
| sharedLibs | rebuild.mk | A list of dynamic libraries to link into this module, ordered for a single-pass linker (dependencies after libraries which depend on them). |
| appName | rebuild.mk | If set, builds the module into an application with this name. |

# Helper Functions #
If you end up needing to write a custom recipe, these may be useful. For
example, to make a directory:
  ```make
  $(ReBuild_buildPath)/special_dir:
      $(q)$(call log,mkdir,$@)
      $(q)$(call run,mkdir -p $@)
  ```
        
| Name | Usage | Description |
|------|-------|-------------|
| log | $(call log,func,message) | Generates console output like - [ func ] message |
| lower | var := $(call lower,STRING) | Returns a lowercase version of STRING. |
| parentdir | var := $(call parentdir,dirname) | Cross-platform method to get the full path name of the parent directory of dirname. |
| run | $(call run,cmd) | From a recipe, executes cmd with the proper verbosity and and log redirection for the quietness setting. |
| upper | var := $(call upper,string) | Returns an uppercase version of string. |

# Predefined Variables #
| Name | Description |
|------|-------------|
| arch | The target architecture. |
| buildarch | The architecture of the system doing the compiling. |
| buildlog | Path to the build log for the current build. |
| buildos | The operating system of the system doing the compiling. |
| comma | Represents a comma (','). |
| debug | Set to 'debug' if building a debug build, blank otherwise. |
| os | The target operating system. |
| platform | The platform tuple: $(arch)-$(vendor)-$(os)-$(toolchain). |
| q | The quiet prefix for build rules. |
| release | Set to 'release' if building a release build, blank otherwise. |
| soext | File extension for dynamic libraries (.so for Linux, .dylib for macOS). |
| space | Represents a space (' '). |
| srctree | Absolute path to the top of the source tree being built. |
| symext | File extension for symbols files (.debug for Linux, .dSYM for macOS).  |
| tag | The build tag - currently one of 'debug', 'release', or 'debugrelease' |
| ReBuild\_buildPath | Relative path to the build root directory. |
| ReBuild\_buildPathInc | Relative path to the build's /inc directory. |
| ReBuild\_buildPathObj | Relative path to the build's /obj directory. |
| ReBuild\_buildPathLib | Relative path to the build's /lib directory. |
| ReBuild\_buildPathBin | Relative path to the build's /bin directory. |
| ReBuild\_buildPathPkg | Relative path to the build's /pkg directory. |
| ReBuild\_gitVersion | If using git, the current git version identifier. |
| ReBuild\_ldSoFlags | Set by a port file to the special linker flag for building a dynamic library. |
| ReBuild\_ldNameFlags | Set by a port file to the special linker flag for setting a dynamic library name. |
| ReBuild\_sdkVer | Set by a port file to the SDK version to build against, if applicable. |
| ReBuild\_sysroot | Set by a port file to the sysroot to build against, if applicable. |


# Port Variables #
| Name | Usage | Description |
|------|-------|-------------|
| ReBuild\_extractSymbols | $(call ReBuild\_extractSymbols,infile,outfile) | Extracts the debug symbols from infile and stores them to outfile. |

