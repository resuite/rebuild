# TI MSP430 Open Source GCC Toolchain #
Port file: [port-msp430-elf-gcc.mk](../../port-msp430-elf-gcc.mk)

This port uses the [TI Open Source MSP430
compiler](https://www.ti.com/tool/MSP430-GCC-OPENSOURCE) as provided by Mitto
Systems.

# Defaults #
This port defaults to generating code optimized for size.

# Port-specific Variables #
## msp430\_cpu ##
Input. Required.

`msp430_cpu` must be set to the full name of the target MSP430 CPU (i.e.
msp430g2452).

## msp430\_toolchain ##
Input. Optional.

Default: /opt/msp430-elf-gcc

`msp430_toolchain` should point to the base of the toolchain directory. It is
recommended to extract the toolcahin into /opt under its original name and
symlink to your preferred version with /opt/msp430-elf-gcc.

However, you can always override this variable to build against a specific
toolchain (see [Notes](#Notes) below).

# Notes #

You can always build your code against a specific compiler version and CPU
either by overriding `msp430_toolchain` and/or `msp430_cpu` during the build
command:

    > make msp430_cpu=msp430g2452 msp430_toolchain=/opt/msp430-gcc-9.3.1.2

Or by hard-coding one or both of the variables into your top-level Makefile (so
that the default is shared across your team and recorded in your version control
system):

    > cat Makefile
    REBUILD_PROJECT=my_msp430_project
    
    msp430_toolchain := /opt/msp430-gcc-9.3.1.2
    msp430_cpu := msp430g2452

    dirs := \
      src

    include rebuild/rebuild.mk

    > make

