# Native ARM64 iOS Clang Toolchain #
Port file: [port-aarch64-darwin-clang.mk](../../port-aarch64-darwin-clang.mk)

This port supports iOS toolchains (iPhone, iPad, etc.).

This port is intended to use whatever Apple developer tools are already
available on your path. It's a quick, easy way to get up and running with a new
project or a project only intended to build with whatever local toolchain is
installed.

# Defaults #
CFLAGS is pre-configured for function and data sections and LDFLAGS is
preconfigured to strip unused functions and data from the compiled binary.

STRIP is pre-configured for the most aggressive symbols stripping possible.

# Port-specific Variables #
## ReBuild\_sdkVer ##
Input. Optional.

ReBuild\_sdkVer should be set to the minimum iOS SDK version you wish to
target. If left unset, it defaults to the latest iOS SDK version installed on
the machine.

## ReBuild\_sysroot ##
Input. Optional.

ReBuild\_sysroot should be set to the SDK install path. If left unset, the path
is determined automatically.

