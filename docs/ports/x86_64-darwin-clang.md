# Native x86\_64 macOS Clang Toolchain #
Port file: [port-x86\_64-darwin-clang.mk](../../port-x86_64-darwin-clang.mk)

This port is intended to use whatever Apple developer tools are already
available on your path. It's a quick, easy way to get up and running with a new
project or a project only intended to build with whatever local toolchain is
installed.

# Defaults #
Function and data sections are enabled and unused sections are stripped to
minimize final binary size.

STRIP is pre-configured for the most aggressive symbols stripping possible.

# Port-specific Variables #
## ReBuild\_sdkVer ##
Input. Optional.

`ReBuild_sdkVer` should be set to the minimum macOS SDK version you wish to
target. If left unset, it defaults to the latest macOS SDK version installed on
the machine.

## ReBuild\_sysroot ##
Input. Optional.

`ReBuild_sysroot` should be set to the SDK install path. If left unset, the path
is determined automatically.
