# Native x86\_64 Linux GCC Toolchain #
Port file: [port-x86\_64-linux-gcc.mk](../../port-x86_64-linux-gcc.mk)

This port is intended to use whatever GCC tools are already available on your
path. It's a quick, easy way to get up and running with a new project or a
project only intended to build with whatever local toolchain is installed.

# Defaults #
CFLAGS is pre-configured for function and data sections and LDFLAGS is
preconfigured to strip unused functions and data from the compiled binary.

STRIP is pre-configured for the most aggressive symbols stripping possible.

# Port-specific Variables #
None.
