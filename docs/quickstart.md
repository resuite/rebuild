[[_TOC_]]

# Quick Start
We're going to set up ReBuild to work with a simple project with a single source
file. We'll be assuming you're using git as your SCM.

## Set Up the Demo Project
First, create a directory for your project. We're going to call it `myapp`.

  ```bash
  mkdir myapp
  cd myapp
  ```

Next, create a directory to contain your source code. By default, ReBuild
assumes all code is contained in subdirectores (which ReBuild considers to be
modules) and not in the root directory.  Since we have only a very simple
project right now, our submodule can just be called `src/`.

  ```bash
  mkdir src
  ```

Now, create the file `src/main.c` and put in the following:

  ```c
  #include <stdio.h>

  int main(void)
  {
      printf("Hello World!\n");
      return 0;
  }
  ```

## Set Up the Project to use ReBuild
Now you need to add the ReBuild git repository as a subdirectory of your project
(or as a git submodule if you already have a project you're working on).

  ```bash
  git clone https://gitlab.com/resuite/rebuild.git
  ```

ReBuild works using GNU Make, so we'll need a Makefile. You can create a
template makefile with ReBuild like so:

  ```bash
  make -f rebuild/rebuild.mk template
  ```

Now you should have a file named Makefile in your root directory that looks
something like this:

  ```make
  ReBuild_project := myapp

  pkgs :=

  dirs :=

  include rebuild/rebuild.mk
  ```

The first line defines the name of your project. `myapp` works just fine for
now.

For now, we'll ignore the `pkgs :=` line. The `pkgs` variable is used to build
external projects, but we won't have any of those in this example. There's no
harm in leaving it there.

The `dirs` variable will contain a list of all submodules in the project. For
now, there's only the one `src` directory so let's add that.

  ```make
  dirs := src
  ```

Finally, the last line hooks into the ReBuild library and generates all of the
necessary build rules and targets. Your final Makefile should look like this:

  ```make
  ReBuild_project := myapp

  pkgs :=

  dirs := src

  include rebuild/rebuild.mk
  ```
## Configure the src/ Module
ReBuild requires a special file called `rebuild.mk` in each submodule. This
file describes what ReBuild should build from the source in the module. By
default, ReBuild assumes you want to build everything in the directory, if
that's true, the configuration is very minimal. Create the file `src/rebuild.mk`
and add the following:

  ```make
  appName := myapp
  ```

That's all you need. The variable `appName` tells ReBuild that this module
contains code which should be compiled into an application named `myapp`.

## Build It
Now you can compile the code.

  ```bash
  make
  ```

ReBuild will create a directory called build/ in the root of your application to
contain all of the build artifacts. Since ReBuild supports having artifacts for
multiple build targets available at the same time, the directory under build/
indicates which toolchain was used to build your application. Under that is your
build 'tag'. And, finally, a directory called bin/ with your applciation. I'm
building on an Intel macOS machine, so my build tree looks like this:

  build/x86\_64-darwin-clang/release/bin/myapp

x86\_64-darwin-clang indicates I've built for 64-bit Intel, on a Darwin kernel
(also known as macOS), with the clang toolchain. And the release/ directory
indicates I built a 'release' version of the application. You should now be able
to execute your application.

  ```bash
  > ./build/x86_64-darwin-clang/release/bin/myapp
  Hello World!
  ```
# Next Steps #
Most projects are broken into multiple submodules or libraries. See how those
work [here](docs/submodules.md).
