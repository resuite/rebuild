# Submodules #
Most projects are eventually built up from multiple components, generally built
as static or dynamic libraries. ReBuild supports building all of your components
and their dependencies with a single build command.

# Organizing Your Workspace #
The general layout of an application with two libraries is as shown below:

```bash
> tree tests/02-modules/
tests/02-modules/
├── app
│   ├── main.c
│   └── rebuild.mk
├── hello
│   ├── hello.c
│   ├── hello.h
│   └── rebuild.mk
├── Makefile
├── rebuild/
│   ├── ...
│   └── ...
└── world
    ├── rebuild.mk
    ├── world.c
    └── world.h
```

Where the main application code is under the `app/` directory and the `hello/`
and `world/` directories each compile to static libraries (.a files) or dynamic
libraries (.so, .dylib, etc.). In this design, your primary components are all
stored in directories peer to your application. This is not a requirement for
ReBuild to function, you can add any directory structure depth to your `dirs`
variable if necessary.

# Configuring the Submodules #
Just like the main app directory, every submodule must contain a `rebuild.mk`
file describing the configuration of that module. Building static and dynamic
libraries is quite simple.

## Configuring a Static Library #
Let's first take the contents of hello/rebuild.mk as an example:

```make
staticLibName := hello
hfiles := hello.h
```

ReBuild assumes any code files in your module should be built so there is no
need to specify the code files themselves unless you require different behavior.

Assigning the variable `staticLibName` serves two purposes. First, the variable
name itself tells ReBuild that we're building a static library (.a file) from
the code in this directory. The second function that it provides the stem of the
library's file name. A static library's filename is typically 'lib', followed by
a name, followed by the extension '.a'. In this case, our stem is 'hello' so our
final library name will be libhello.a.

Assigning the variable `hfiles` tells ReBuild which header files from your
module are public. You should only list public header files here. Any files
listed in `hfiles` are copied to build/\<platform\>/include and made accessible
to all other modules in the project. By default, ReBuild assumes all header
files are private, which is why you must specify any headers which describe
public interfaces into your module.

## Configuring a Dynamic Library ##
In our example, the world/ module gets compiled into a shared library. Let's
have a look at the rebuild.mk file:

```make
sharedLibName := world
hfiles := world.h
```

This make fragment looks nearly identical, save for the first variable name -
`sharedLibName`. In this case, we're building a shared library (a.k.a dynamic
library) and so we use the name `sharedLibName` instead of `staticLibName` to
inform ReBuild to link the compiled files into a dynamic library. ReBuild uses
whatever dynamic file format is correct for your platform (.so, .dylib, etc.)
and names the final target binary accordingly (in this case, libworld.so on
Linux).

Since shared libraries are linked, you may also require static or dynamic
dependencies for a shared library. This can be accomplished in the same way as
configuring the main app to use libhello.a and libworld.so as shown be low (the
`staticLibs` and `sharedLibs` variables).

## Configuring the Application ##
Lastly, let's have a look at app/rebuild.mk:

```make
appName := myapp

staticLibs := \
  hello

sharedLibs := \
  world
```

Here we've added the use of the `staticLibs` and `sharedLibs` variables. These
variables are whitespace delimited lists of the stem names of any library
dependencies. ReBuild will attempt to link these libraries when generating your
final target binary.

One important thing to note is that the libraries should be listed in the same
order you'd list them for a single-pass linker. That is, any libraries with
dependencies should be listed before their dependencies.

ReBuild does its best to ensure that if there are both static and shared
libraries with the same name, that the correct one is linked according to what
is indicated by either the `staticLibs` or `sharedLibs` variables. However, not
all toolchains play nice. Apple's version of clang for macOS and iOS, for
instance, will ignore any static libraries in favor of dynamic ones, even when
instructed to do otherwise. If it's important to link a static library over a
dynamic one, then you should avoid building the dynamic one where possible,
or consider renaming the dynamic one, if you have that option.

# Building the Project #
Lastly, we need to tell the project to compile these new directories. Updating
the Makefile is pretty simple:

```make
ReBuild_project := myapp

pkgs :=

dirs := \
  world \
  hello \
  app

include rebuild/rebuild.mk
```

In this example, we've simply expanded the `dirs` variable to include our new
submodules. The modules in the `dirs` variable should be ordered such that the
modules with dependencies are listed after their dependencies. This helps
ReBuild perform parallel builds correctly.

