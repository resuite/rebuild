# Overview #
ReBuild exposes a number of ReBuild configuration items as defines so they can
be used by your build toolchains.

| Name | Description |
|------|-------------|
| REBUILD\_PROJECT\_STRING | The make variable ReBuild\_project as a string. |
| REBUILD\_ARCH\_XXX | Defined where XXX is the uppercase name of the target architecture. |
| REBUILD\_ARCH\_STRING | Target architecture as a string. |
| REBUILD\_GIT\_VERSION | Git version string (if using git). |
| REBUILD\_OS\_XXX | Defined where XXX is the uppercase name of the target operating system. |
| REBUILD\_OS\_STRING | Target operating system as a string. |
| REBUILD\_TAG\_STRING | Build tag as a string. |
