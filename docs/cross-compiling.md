# Cross-Compiling #
Cross-compiling with ReBuild is minimally different than compiling natively. The
first step is understanding ReBuild's name for your platform. The list of known
platforms can be found by invoking `make help` and looking at the list at the
bottom:

```bash
> make help
Buildling myapp x86_64-linux/release

General usage: make [tag=debug|release|debugrelease] [arch=ARCH] [os=OS] [quiet=yes|no]

ReBuild defaults to quiet builds (quiet=yes) with a release tag (tag=release)
built for the build machine's architecture and operating system. You can
override this on the command line as shown above. For instance, to have a very
verbose output built with the release tag on the ARM architecture:

    > make tag=debug arch=arm quiet=no

Currently supported p:
  aarch64-darwin-clang
  msp430-elf-gcc
  x86_64-darwin-clang
  x86_64-linux-gcc
```

In this case, ReBuild understand four different platforms. The platforms are
named with either three or four components in the following format:

    <arch>-<os>-<toolchain>

or 

    <arch>-<vendor>-<os>-<toolchain>

Note that the MSP430 port is weird. Generally, code for the MSP430 is written
without an OS, so there's nothing to put into the OS field here. Mitto Systems -
the current maintainers of the MSP430 open source toolchain - have, for some
reason, named their compiler msp430-elf, so that name was replicated in the
MSP430 port.

You can select a platform by specifying some or all of the components using
build-line or Makefile variables:

    make arch=aarch64 os=darwin toolchain=clang

or

    > cat Makefile
    REBUILD_PROJECT := my_project
    arch := aarch64
    os := darwin
    toolchain := clang
    ...
    > make

Any variables left unset are inferred from the environment if possible. For
example, on an x86\_64 macOS system if you're building for the native
environment, none of those variables must be specified. ReBuild will figure them
all out. If, however, you want to build for the same environment, but for
32-bit, you can specify just the `arch` variable and let ReBuild infer the rest:

    make arch=i386

This is helpful if you're building portable code for multiple platforms. If
you're only ever building code for a single platform - say an embedded platform -
then it might be more useful to define your toolchain variables right in the top
level Makefile.
