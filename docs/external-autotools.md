[[_TOC_]]

# Compiling External Autotools Projects
It's possible to use ReBuild to compile external Autotools based projects as
long as you have all of the necessary Autotools applications installed.

First, make the project a sub module of your ReBuild project. For our examples
I'll assume you're using git and we'll use zlib and libyaml to show two
different kinds of Autotools builds.

  ```bash
  git submodule add https://github.com/madler/zlib.git
  git submodule add https://github.com/yaml/libyaml.git
  ```

Next, add the sub modules to the `pkgs` variable in your Makefile:

  ```make
  pkgs += zlib
  pkgs += libyaml
  ```

By default, ReBuild assumes a certain standard of Autotools. libyaml follows
this standard, so we'll start with that. For a project meeting ReBuild's
standards, the only thing we need to do to get the project to build is to tell
ReBuild what artifacts it should expect to see come from the project. In our
example, the only thing we care about is the libyaml static library. For this we
use the libyaml\_TARGET variable. Once you define a package in the pkgs
variable, ReBuild will look for variables with that package name followed by
various suffixes such as TARGET and CONFIGURE\_FLAGS.

  ```make
  pkgs += libyaml
  libyaml_CONFIGURE_FLAGS := --disable-shared
  libyaml_TARGET := src/.libs/libyaml.a
  ```

zlib doesn't conform to the Autotools standard expected by ReBuild. Namely, it
doesn't support the Autotools --host flag for cross compiling. As such, we have
more work to do. ReBuild lets you override the configure command. When you do
that, ReBuild doesn't set any configure flags by default. To override the
configure command you use the \<pkg\>\_CONFIGURE variable. In our case, zlib
still uses configure, so we'll set that variable back to 'configure'.

  ```make
  pkgs += zlib
  zlib_CONFIGURE := configure
  zlib_CONFIGURE_FLAGS = --const --prefix=$(CURDIR)/$(ReBuild_buildPath) --includedir=$(CURDIR)/$(ReBuild_buildPathInc)
  zlib_TARGET := libz.a
  ```
Note that zlib\_CONFIGURE\_FLAGS is assigned with '=' and not ':='. This is not
a mistake. The variables 'CURDIR', 'ReBuild\_buildPath', and
'ReBuild\_buildPathInc' aren't set yet in the Makefile. They will be set once
you include rebuild/rebuild.mk. So, we set the variable with '=' so that it's
evaulated when the rule is executed. At that point, the correct variable values
will be set. Putting it together, our Makefile looks like this:

  ```make
  ReBuild_project := myapp

  pkgs += zlib
  zlib_CONFIGURE := configure
  zlib_CONFIGURE_FLAGS = --const --prefix=$(CURDIR)/$(ReBuild_buildPath) --includedir=$(CURDIR)/$(ReBuild_buildPathInc)
  zlib_TARGET := libz.a

  pkgs += libyaml
  libyaml_CONFIGURE_FLAGS := --disable-shared
  libyaml_TARGET := src/.libs/libyaml.a

  include rebuild/rebuild.mk
  ```

## Package Specific Flags
libyaml generates a few warnings on my x86-linux-alpine test system. Since I'm
not going to fix libyaml's issues and I don't want to see them intermingled with
my legitimate warnings and errors, I can pass some package specific CFLAGS to
Autotools using the libyaml\_CFLAGS variable.

  ```make
  pkgs += libyaml
  libyaml_CONFIGURE_FLAGS := --disable-shared
  libyaml_TARGET := src/.libs/libyaml.a
  libyaml_CFLAGS := -Wno-unused-value
  ```

Running `make clean` and then `make` gives me a silent build. 

You can also specifiy package specific CPPFLAGS and LDFLAGS. 

