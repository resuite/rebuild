.SECONDEXPANSION:
.DELETE_ON_ERROR:
.SUFFIXES:

# TODO: This currently works with bash and zsh. Probably need an extensible
# mechanism to handle different shells (i.e. tcsh, ash, busybox)
SHELL := $(shell which bash)
.SHELLFLAGS := -ceuo pipefail

################################################################################
# Some Helper Functions
################################################################################
lower = $(shell echo $1 | tr '[A-Z]' '[a-z]')
upper = $(shell echo $1 | tr '[a-z]' '[A-Z]')
# $(call log,level,message)
log = printf "  [ %9s ] %s\n" $1 $(notdir $2)
nop :=
space := $(nop) $(nop)
comma :=,
parentdir = $(realpath $1/..)

################################################################################
# Collect Platform Info
################################################################################
buildarch := $(call lower,$(shell uname -m))
buildos := $(call lower,$(shell uname -s))

################################################################################
# Set Default Options
################################################################################
arch ?= $(buildarch)
os ?= $(buildos)
# Apple products default to clang, other *nixes (currently) default to gcc
toolchain ?= $(if $(filter darwin,$(os)),clang,gcc)
tag ?= $(if $(REBUILD_DEFAULT_TAG),$(REBUILD_DEFAULT_TAG),release)

cstd ?= gnu11
cxxstd ?= c++17

MAKEFLAGS += $(if $(filter no,$(quiet)),,--silent)
srctree := $(realpath $(dir $(firstword $(MAKEFILE_LIST))))

debug := $(filter debug,$(tag))
release := $(filter release,$(tag))
q := $(if $(filter --silent s,$(MAKEFLAGS)),@)

ifeq (,$(filter $(tag),debug release debugrelease))
  $(error Unsupported tag '$(tag)'. Pick one of: debug, release, or debugrelease)
endif

platform := $(subst $(space),-,$(strip $(arch) $(vendor) $(os) $(toolchain)))
include rebuild/port-$(platform).mk
include rebuild/checktoolchain.mk
platform := $(subst $(space),-,$(strip $(arch) $(vendor) $(os) $(toolchain)))

export CC
export CPP
export LD

ifeq (clean,$(MAKECMDGOALS))
  $(info Cleaning $(ReBuild_project))
else
  $(info Buildling $(ReBuild_project) $(arch)-$(os)/$(tag))
endif

soext ?= so
symext ?= debug

ReBuild_buildPath := build/$(platform)/$(tag)
ReBuild_buildPathDoc := build/doc
ReBuild_buildPathInc := $(ReBuild_buildPath)/../inc
ReBuild_buildPathObj := $(ReBuild_buildPath)/obj
ReBuild_buildPathLib := $(ReBuild_buildPath)/lib
ReBuild_buildPathBin := $(ReBuild_buildPath)/bin
ReBuild_buildPathPkg := $(ReBuild_buildPath)/pkg

$(shell rm -f $(buildlog))
buildlog := $(ReBuild_buildPath)/build.log
run = $(if $(q),{ echo "$1"; $1; } >>,$1 | tee -a) $(CURDIR)/$(buildlog)

# TODO: Add git revision if git is installed.
CPPFLAGS += \
  -I$(ReBuild_buildPathInc) \
  -DREBUILD_PROJECT_STRING="\"$(ReBuild_project)\"" \
  -DREBUILD_ARCH_$(call upper,$(arch)) \
  -DREBUILD_ARCH_STRING="\"$(arch)\"" \
  -DREBUILD_OS_$(call upper,$(os)) \
  -DREBUILD_OS_STRING="\"$(os)\"" \
  -DREBUILD_TAG_STRING="\"$(tag)\""

ifeq ($(wildcard .git),.git)
  ReBuild_gitVersion := $(shell git describe --dirty --abbrev=8 2>/dev/null)
  ifeq ($(ReBuild_gitVersion),)
    ReBuild_gitVersion := $(shell git describe --all --long --abbrev=8)
  endif
  CPPFLAGS += -DREBUILD_GIT_VERSION="\"$(ReBuild_gitVersion)\""
endif

CFLAGS += \
  -Wall \
  -Wpedantic \
  -g \
  -fpic \
  -fPIC

ifeq ($(tag),debug)
  CPPFLAGS += \
    -DDEBUG=1

  CFLAGS += \
    -O0

else ifeq ($(tag),release)
  CPPFLAGS += \
    -DNDEBUG=1 \
	-DRELEASE=1

  CFLAGS += \
    -O2

else ifeq ($(tag),debugrelease)
  CPPFLAGS += \
    -DDEBUG=1 \
	-DRELEASE=1

  CFLAGS += \
    -O2
endif

define ReBuild_recurse
  dirs :=
  include $(cpath)/rebuild.mk
  include rebuild/buildrules.mk
endef	# ReBuild_recurse

.DEFAULT_GOAL := all
.PHONY: all
all: | $(dir $(buildlog))

.PRECIOUS: %/.marker
%/.marker:
	$(q)mkdir -p $(@D)
	$(q)touch $@

# ReBuild_publishHeader
# $1 - Module path (cpath)
# $2 - Public header directory
# $3 - Header path within module
define ReBuild_publishHeader
$(ReBuild_buildPathInc)/$(if $2,${2}/)%.h: ${1}/$(if $3,${3}/)%.h $$$$(@D)/.marker
	$$(q)$$(call log,publish,$$<)
	$$(q)$$(call run,cp $$< $$@)
endef	# ReBuild_publishHeader

$(dir $(buildlog)):
	$(q)mkdir -p $@

define ReBuild_cleanTarget
clean_$1:
	$(q)$2
clean: clean_$1
endef

ReBuild_cleanTargets := $(foreach pkg,$(pkgs),$(if $(filter make,$($(pkg)_BUILDTYPE)),$(pkg)))
$(foreach pkg,$(ReBuild_cleanTargets),$(eval $(call ReBuild_cleanTarget,$(pkg),if [ -e $(pkg) ]; then cd $(pkg) && make clean; fi;)))

.PHONY: clean
clean:
	$(q)find . -name build -type d -exec rm -rf {} 2>/dev/null \; || true

# TODO: Help
.PHONY: help
help:
	$(q)cat rebuild/templates/help.txt
	$(q)for f in `ls rebuild/port-*.mk | grep -v core.mk`; do \
		printf "  "; \
	   	basename $$f .mk | cut -d '-' -f 2-; \
		done

.PHONY: template
template:
	$(q)if [ -e Makefile ]; then \
			echo "Existing Makefile detected, template will be Makefile.rebuild"; \
			cp rebuild/templates/Makefile Makefile.rebuild; \
		else \
			cp rebuild/templates/Makefile Makefile; \
		fi
	$(q)if ! grep build/ .gitignore >/dev/null 2>&1; then \
			echo build/ >> .gitignore; \
		fi


################################################################################
# Rules to build documentation:
# 	1. If docs == "no", we don't build docs. This is the master override.
# 	2. If doxygen is set or a Doxyfile is found, build Doxygen docs.
# 	3. TBD for other documentation types
# 	4. If no documentation files were set or found, don't build docs.
#
# Doxygen can integrate with ReBuild using the below environment variables.
################################################################################
doxygen_file := $(if $(doxyfile),$(doxyfile),$(wildcard Doxyfile))
doxygen_pedantic ?= $(if $(filter release,$(tag)),YES,NO)

.PHONY: doxygen
doxygen:
	$(q)$(call log,doxygen,$(doxyfile))
	$(q)$(call run,\
		REUSE_DOX_VERBOSE=$(doxygen_pedantic) \
		doxygen $(doxygen_file) $(if $(filter --silent s,$(MAKEFLAGS)),-q))

ifneq (no,$(docs))
  ifneq (,$(doxygen_file))
all: doxygen
  endif
endif

$(foreach pkg,$(pkgs),$(eval include rebuild/buildpkg.mk))

$(foreach cpath,$(dirs),$(eval $(call ReBuild_recurse)))

