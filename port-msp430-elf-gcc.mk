msp430_toolchain ?= /opt/msp430-elf-gcc

ifeq ($(msp430_cpu),)
  $(error You must specify msp430_cpu.)
endif

msp430_cpu := $(call lower,$(msp430_cpu))

CPPFLAGS += \
  -I$(msp430_toolchain)/include

CFLAGS += \
  -mmcu=$(msp430_cpu) \
  -Os

LDFLAGS += \
  -Wl,--gc-sections \
  -mmcu=$(msp430_cpu) \
  -L$(msp430_toolchain)/lib \
  -L$(msp430_toolchain)/include

OBJCOPY := $(msp430_toolchain)/bin/msp430-elf-objcopy

ReBuild_extractSymbols = $(OBJCOPY) --only-keep-debug $1 $2
ReBuild_linkSymbols = $(OBJCOPY) --add-gnu-debuglink=$1 $2

ReBuild_ldSoFlags := -shared
ReBuild_ldNameFlags := -Wl,soname

CC := $(msp430_toolchain)/bin/msp430-elf-gcc
CPP := $(msp430_toolchain)/bin/msp430-elf-cpp
CXX := $(msp430_toolchain)/bin/msp430-elf-c++
LD := $(msp430_toolchain)/bin/msp430-elf-ld
AR := $(msp430_toolchain)/bin/msp430-elf-ar
STRIP := $(msp430_toolchain)/bin/msp430-elf-strip
GDB := $(msp430_toolchain)/bin/msp430-elf-gdb
GDB_AGENT_CONSOLE := $(msp430_toolchain)/bin/gdb_agent_console
