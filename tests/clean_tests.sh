#! /bin/sh
for d in `ls -d */ | grep -E '[0-9]{2}-' | grep -v '\-disable'`; do
    echo "Cleaning $d"
    cd "$d"
    make clean > /dev/null
    if [ -e unprep.sh ]; then
        ./unprep.sh
    fi
    cd - > /dev/null
done
