#! /bin/bash
set -eE
trap 'printf "\e[31m%s: code %s\e[m\n" "  ** TEST FAILED **" $?' ERR

for d in `ls -d */ | grep -E '[0-9]{2}-' | grep -v '\-disable'`; do
    echo "Running $d"
    cd $d
    if [ -e prep.sh ]; then
        ./prep.sh
    fi
    make -j > /dev/null
    app=`find build/ -name myapp`
    lib_path=$(dirname $(dirname ${app}))/lib
    LD_LIBRARY_PATH=${lib_path} ${app} | grep "Hello World" > /dev/null
    lines=$(make -j | wc -l)
    if [ ${lines} -ne 1 ]; then
        exit 2
    fi
    cd - > /dev/null
done
