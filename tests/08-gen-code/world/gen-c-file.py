#! /usr/bin/env python3
from argparse import ArgumentParser

code = r'''
#include <stdio.h>

#include <world.h>

void world(void)
{
    printf("World!\n");
}
'''

parser = ArgumentParser()
parser.add_argument('out')

args = parser.parse_args()

with open(args.out, 'w') as f:
    f.write(code)
