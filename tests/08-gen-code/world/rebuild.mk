sharedLibName := world

srcdir := build/gen
incdir := build/gen

cfilesgen := $(cpath)/$(srcdir)/world.c
hfilesgen := $(cpath)/$(srcdir)/world.h

.PRECIOUS: $(cpath)/$(srcdir)/%.c
$(cpath)/$(srcdir)/%.c: $(cpath)/gen-c-file.py $$(@D)/.marker
	$(q)$(call log,gen-c,$<)
	$(q)$(call run,$< $@)

.PRECIOUS: $(cpath)/$(srcdir)/%.h
$(cpath)/$(srcdir)/%.h: $(cpath)/gen-h-file.py $$(@D)/.marker
	$(q)$(call log,gen-h,$<)
	$(q)$(call run,$< $@)

