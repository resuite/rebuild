#! /usr/bin/env python3
from argparse import ArgumentParser

code = r'''
#ifndef WORLD_H
#define WORLD_H

void world(void);

#endif  /* !WORLD_H */
'''

parser = ArgumentParser()
parser.add_argument('out')

args = parser.parse_args()

with open(args.out, 'w') as f:
    f.write(code)
