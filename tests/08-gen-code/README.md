# Test: 08-gen-code #
Purpose: Test generating code to be compiled.

Module 1: hello
Produces: A static library (.a file)
Code: Static

Module 2: world
Produces: A static library (.a file)
Code: Dynamically generated

Module 3: app
Produces: An executable
Code: Static
Depends on:
 - libhello.a from hello/
 - libworld.a from world/
