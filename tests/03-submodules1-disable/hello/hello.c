#include <stdio.h>

#include "hello.h"
#include "world/world.h"

void hello(void)
{
    printf("Hello ");
}

void hello_world(void)
{
    hello();
    world();
}
