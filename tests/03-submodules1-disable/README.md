# Test: 03-submodules1 #
Purpose: Test compiling a sub-sub static library into a sub shared library.

Module 1: hello/world
Produces: A static library (.a file)

Module 2: hello
Produces: A shared library (.so file)
Depends on: libworld.a from hello/world/

Module 3: app
Produces: An executable file.
Depends on: libhello.so from hello/
