# Test: 02-modules #
Purpose: Test compiling three modules into an application.

Module 1: hello
Produces: A static library (.a file)

Module 2: world
Produces: A shared library (.so file)

Module 3: app
Produces: An executable, linking to modules 1 and 2.
Depends on:
 - libhello.a from hello/
 - libworld.so from world/
