#include <hello.h>
#include <world.h>

int main(int argc, char** argv)
{
    (void)argc;
    (void)argv;
    hello();
    world();
    return 0;
}
