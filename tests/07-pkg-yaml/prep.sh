#! /bin/sh
if [ ! -e libyaml ]; then
    mkdir -p libyaml 
    echo "  Fetching libyaml..."
    curl -L https://github.com/yaml/libyaml/tarball/0.2.5 2>/dev/null | tar -xz -C libyaml --strip-components 1
fi
