# Test: 07-pkg-yaml #
Purpose: Test compiling external libraries with modern configure scripts.

Module 1: libyaml (created by prep.sh)
Produces: A static library (.a file)
Compile Method: ./configure && make && make install

Module 2: app
Produces: An executable
Compile Method: make/ReBuild
Depends on: libyaml.a from libyaml/
