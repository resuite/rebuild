#include <stdio.h>
#include <zlib.h>

static unsigned char src[] = "This is my test data.";
static unsigned char dst[1024];

int main(int argc, char** argv)
{
    unsigned long size;
    (void)argc;
    (void)argv;

    size = sizeof(dst);
    compress(dst, &size, src, sizeof(src));
    printf("Hello World!\n");
    return 0;
}
