# Test: 06-pkg-zip #
Purpose: Test compiling external libraries with old/broken configure scripts.

Module 1: zlib (created by prep.sh)
Produces: A static library (.a file)
Compile Method: ./configure && make && make install

Module 2: app
Produces: An executable
Compile Method: make/ReBuild
Depends on: libz.a from zlib/
