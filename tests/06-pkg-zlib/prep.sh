#! /bin/sh
if [ ! -e zlib ]; then
    mkdir -p zlib
    echo "  Fetching zlib..."
    curl -L https://github.com/madler/zlib/tarball/v1.2.11 2>/dev/null | tar -xz -C zlib --strip-components 1
fi
