staticLibName := hello

srcdir := ../printmsg
hfiles := hello.h

extraCPPFLAGS := \
  -DPRINTMSG_FUNCTION_NAME=hello \
  -DPRINTMSG_MESSAGE='"Hello "'
