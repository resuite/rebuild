staticLibName := world

srcdir := ../printmsg
hfiles := world.h

extraCPPFLAGS := \
  -DPRINTMSG_FUNCTION_NAME=world \
  -DPRINTMSG_MESSAGE='"World!\n"'
