# Test: 05-multi-compile-module #
Purpose:
 - Test compiling the same source code multiple times into different libraries
 - Test module-specific compilation flags (CPPFLAGS)

Source Location: printmsg
Compiled by:
 - hello/
 - world/

Module 1: hello
Produces: A static library (.a file)
Compiles: printmsg
Flags: CPPFLAGS -> renames the function in printmsg.c to 'hello()'

Module 2: world
Produces: A static library (.a file)
Compiles: printmsg
Flags: CPPFLAGS -> renames the function in printmsg.c to 'world()'

Module 3: app
Produces: An executable
Depends on:
 - libhello.a from hello/
 - libworld.a from world/
