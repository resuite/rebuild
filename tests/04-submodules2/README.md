# Test: 04-submodules2 #
Purpose: Test compiling sub and sub-sub modules all as static libraries.

Module 1: hello/world
Produces: A static library (.a file)

Module 2: hello
Produces: A static library (.a file)

Module 3: app
Produces: An executable
Depends on:
 - libhello.a from hello/
 - libworld.a from hello/world/
