#include <stdio.h>

#include "world/world.h"

static void hello(void)
{
    printf("Hello ");
}

void hello_world(void)
{
    hello();
    world();
}
