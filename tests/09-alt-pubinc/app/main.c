#include <hello/hello.h>
#include <world/world.h>

int main(int argc, char** argv)
{
    (void)argc;
    (void)argv;
    hello();
    world();
    return 0;
}
