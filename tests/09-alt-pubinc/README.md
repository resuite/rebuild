# Test: 09-alt-pubinc #
Purpose: Test exporting headers into a module subdirectory in build/.

Module 1: hello
Produces: A static library (.a file)
Exports headers to: build/.../inc/hello

Module 2: world
Produces: A static library (.a file)
Exports headers to: build/.../inc/world

Module 3: app
Produces: An executable
Depends on:
 - libhello.a from hello/
 - libworld.a from world/
