ReBuild_sdkVer ?= $(shell xcrun --find --sdk $(darwin_platform) --show-sdk-version)
ReBuild_sysroot ?= $(shell xcrun --find --sdk $(darwin_platform) --show-sdk-path)

ifeq ($(arch),aarch64)
  darwin_arch := arm64
else
  darwin_arch := $(arch)
endif

soext := dylib
symext := dSYM

CFLAGS += \
  -Wunused-parameter \
  -ffunction-sections \
  -fdata-sections \

LDFLAGS += \
  -dead_strip \
  -lSystem \
  -lc++

ReBuild_extractSymbols = dsymutil $1 -o $2
ReBuild_linkSymbols = echo Symbols auto-linked on macOS

ReBuild_ldSoFlags := -dynamiclib
ReBuild_ldNameFlags := -install_name 

CC := $(shell xcrun --find --sdk $(darwin_platform) clang) -arch $(darwin_arch) -isysroot $(ReBuild_sysroot)
CPP := $(shell xcrun --find --sdk $(darwin_platform) cpp)
CXX := $(shell xcrun --find --sdk $(darwin_platform) c++) -arch $(darwin_arch) -isysroot $(ReBuild_sysroot)
AR := $(shell xcrun --find --sdk $(darwin_platform) ar)
LD := $(shell xcrun --find --sdk $(darwin_platform) ld) -arch $(darwin_arch) -syslibroot $(ReBuild_sysroot)
STRIP := $(shell xcrun --find --sdk $(darwin_platform) strip) -r -S -x
