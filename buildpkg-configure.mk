ifeq ($($(pkg)_CONFIGURE),)

$(ReBuild_buildPathPkg)/$(pkg)/rebuild.configured: $$(@D)/.marker
	$(q)$(call log,configure,$(pkg))
	$(q)$(call run,cd $(@D); \
		CPPFLAGS="$(CPPFLAGS) $($(pkg)_CPPFLAGS)" \
		CFLAGS="$(CFLAGS) $($(pkg)_CFLAGS)" \
		LDFLAGS="$(LDFLAGS) $($(pkg)_LDFLAGS)" \
		$(CURDIR)/$(pkg)/configure \
		--prefix=$(CURDIR)/$(ReBuild_buildPath) \
		--includedir=$(CURDIR)/$(ReBuild_buildPathInc) \
		--host $(arch)-$(os) \
		$($(pkg)_CONFIGURE_FLAGS) \
		&& touch $(@F))

else

$(ReBuild_buildPathPkg)/$(pkg)/rebuild.configured: $$(@D)/.marker
	$(q)$(call log,configure,$(pkg))
	$(q)$(call run,cd $(@D); \
		CPPFLAGS="$(CPPFLAGS) $($(pkg)_CPPFLAGS)" \
		CFLAGS="$(CFLAGS) $($(pkg)_CFLAGS)" \
		LDFLAGS="$(LDFLAGS) $($(pkg)_LDFLAGS)" \
		$(CURDIR)/$(pkg)/$($(pkg)_CONFIGURE) $($(pkg)_CONFIGURE_FLAGS) && touch $(@F))

endif	# <pkg>_CONFIGURE

ifeq ($($(pkg)_TARGET),)
  $(error $(pkg) defined without $(pkg)_TARGET.)
endif

$(addprefix $(ReBuild_buildPathPkg)/$(pkg)/,$($(pkg)_TARGET)): $(ReBuild_buildPathPkg)/$(pkg)/rebuild.configured $$(@D)/.marker
	$(q)$(call log,compile,$(pkg))
	$(q)$(call run,cd $(dir $<); $(MAKE))

$(addprefix $(ReBuild_buildPathLib)/,$(notdir $($(pkg)_TARGET))): pkg := $(pkg)
$(addprefix $(ReBuild_buildPathLib)/,$(notdir $($(pkg)_TARGET))): $(addprefix $(ReBuild_buildPathPkg)/$(pkg)/,$($(pkg)_TARGET))
	$(q)$(call log,publish,$(pkg))
	$(q)$(call run,cd $(ReBuild_buildPathPkg)/$(pkg); $(MAKE) install)

ReBuild_allPkgTargets := $(ReBuild_allPkgTargets) $(addprefix $(ReBuild_buildPathLib)/,$(notdir $($(pkg)_TARGET)))
all: $(addprefix $(ReBuild_buildPathLib)/,$(notdir $($(pkg)_TARGET)))
