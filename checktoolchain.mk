assert_set = $(if $($1),,$(error Toolchain Error: $1 not defined))

$(call assert_set,CC)
$(call assert_set,CPP)
$(call assert_set,CXX)
$(call assert_set,LD)
$(call assert_set,AR)
$(call assert_set,STRIP)
$(call assert_set,ReBuild_extractSymbols)
