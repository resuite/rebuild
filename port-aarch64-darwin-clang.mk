darwin_platform := iphoneos

include rebuild/port-darwin-core.mk

CPPFLAGS += -mios-version-min=$(ReBuild_sdkVer)
CFLAGS += -mios-version-min=$(ReBuild_sdkVer)
LDFLAGS += -mios-version-min=$(ReBuild_sdkVer)

