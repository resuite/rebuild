CFLAGS += \
  -ffunction-sections \
  -fdata-sections \

LDFLAGS += \
  -Wl,--gc-sections

ReBuild_extractSymbols = objcopy --only-keep-debug $1 $2
ReBuild_linkSymbols = objcopy --add-gnu-debuglink=$1 $2

ReBuild_ldSoFlags := -shared
ReBuild_ldNameFlags := -Wl,soname

STRIP := strip -s
